﻿using System;
using System.Net;
using System.Windows;

namespace Get_Ip_DnsWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private async void GetButtonClick(object sender, RoutedEventArgs e)
        {
            if (dnsTextBox.Text.Length == 0 && ipAddressTextBox.Text.Length > 0)
            {
                dnsTextBox.Text = "";

                try
                {
                    var hostInfo = await Dns.GetHostEntryAsync(ipAddressTextBox.Text);
                    dnsTextBox.Text = hostInfo.HostName;
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
            else if (dnsTextBox.Text.Length > 0 && ipAddressTextBox.Text.Length == 0)
            {
                ipAddressTextBox.Text = "";

                try
                {
                    var hostInfo = await Dns.GetHostEntryAsync(dnsTextBox.Text);
                    foreach (var ipAddress in hostInfo.AddressList)
                    {
                        ipAddressTextBox.Text += $"{ipAddress}\n";
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
            else
            {
                MessageBox.Show("Только один из них может быть пустым!");
            }
        }
    }
}
